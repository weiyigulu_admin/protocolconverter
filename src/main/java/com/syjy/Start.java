package com.syjy;

import com.syjy.container.ProtocolDataContainer;
import com.syjy.container.ProtocolTunnelContainer;
import com.syjy.container.RecurringTaskContainer;
import com.syjy.tunnelinfo.BaseTunnelInfo;

import java.util.List;

/**
 * 启动类
 *
 * @author: xiuwei
 * @version:
 */
public class Start {


    public static void main(String[] args) throws Exception {
        List<BaseTunnelInfo> tunnelInfos = ReadConfigFile.readConfigFile();
        for (BaseTunnelInfo tunnelInfo : tunnelInfos) {
            TunnelBuilder.buildTunnel(tunnelInfo).startTunnel();
        }
        RecurringTaskContainer.getInstance().addRecurringTask(10, "打印实时数据入文件", () -> {
            try {
                FileUtils.write2File(FileUtils.getResourcesPath(), "tunnelStatus.log", ProtocolTunnelContainer.getInstance().toStringFormatted());
            } catch (Exception e) {
                e.printStackTrace();
            }

            FileUtils.write2File(FileUtils.getResourcesPath(), "realTimeData.log", ProtocolDataContainer.getInstance().toStringFormatted());
            return null;
        });
    }

}
