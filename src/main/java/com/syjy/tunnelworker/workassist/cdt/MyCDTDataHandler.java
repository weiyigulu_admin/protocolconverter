package com.syjy.tunnelworker.workassist.cdt;

import com.syjy.container.ProtocolDataContainer;
import com.syjy.tunnelinfo.TunnelStatus;
import com.syjy.tunnelworker.gathers.iml.CdtRtuDataGather;
import lombok.NonNull;
import wei.yigulu.cdt.cdtframe.AbstractCDTDataHandler;
import wei.yigulu.cdt.cdtframe.BaseDateType;

import java.math.BigDecimal;
import java.util.List;

/**
 * 基于数据模块的cdt的数据处理类
 *
 * @author: xiuwei
 * @version:
 */
public class MyCDTDataHandler extends AbstractCDTDataHandler {

	ProtocolDataContainer protocolDataContainer = ProtocolDataContainer.getInstance();


	/**
	 * cdt rtu 的数据采集对象 用于采集数据
	 */
	private CdtRtuDataGather cdtRtuDataGather;

	/**
	 * 构造方法
	 *
	 * @param cdtRtuDataGather cdt 采集通道实体
	 */
	public MyCDTDataHandler(@NonNull CdtRtuDataGather cdtRtuDataGather) {
		this.cdtRtuDataGather = cdtRtuDataGather;
	}


	@Override
	protected void processYx(List<BaseDateType> dates) {
		for (BaseDateType dateType : dates) {
			dateType.getDates().forEach((k, v) -> {
				if (this.cdtRtuDataGather.getCdtYxDataPoints().containsKey(k)) {
					protocolDataContainer.putBoolean(this.cdtRtuDataGather.getCdtYxDataPoints().get(k).getId(), (Boolean) v);
				}
			});
		}
	}

	/**
	 * 不分重要 次要和普通 统一处理遥测
	 *
	 * @param dates
	 */
	private void processYc(List<BaseDateType> dates) {
		for (BaseDateType dateType : dates) {
			getLog().info("接收到数据：" + dateType.getDates());
			dateType.getDates().forEach((k, v) -> {
				if (this.cdtRtuDataGather.getCdtYcDataPoints().containsKey(k)) {
					protocolDataContainer.putNumber(this.cdtRtuDataGather.getCdtYcDataPoints().get(k).getId(), BigDecimal.valueOf((Integer) v).multiply(BigDecimal.valueOf(this.cdtRtuDataGather.getCdtYcDataPoints().get(k).getMag())));
				}
			});
		}
	}


	@Override
	protected void processImportantYc(List<BaseDateType> list) {
		processYc(list);
	}

	@Override
	protected void processSecondYc(List<BaseDateType> list) {
		processYc(list);
	}

	@Override
	protected void processCommonYc(List<BaseDateType> list) {
		processYc(list);
	}

	@Override
	public void connected() {
		this.cdtRtuDataGather.setTunnelStatus(TunnelStatus.LISTENSERIALSUCCESS);
	}


	@Override
	public void disconnected() {
		this.cdtRtuDataGather.setTunnelStatus(TunnelStatus.LISTENSERIALFAIL);
	}
}
