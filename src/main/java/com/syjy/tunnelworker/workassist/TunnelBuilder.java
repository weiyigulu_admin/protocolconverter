package com.syjy.tunnelworker.workassist;


import com.syjy.DataExchangeException;
import com.syjy.tunnelinfo.BaseTunnelInfo;
import com.syjy.tunnelinfo.gathertunnelinfo.Gather104TcpTunnelInfo;
import com.syjy.tunnelinfo.gathertunnelinfo.GatherCdtRtuTunnelInfo;
import com.syjy.tunnelinfo.gathertunnelinfo.GatherModbusRtuTunnelInfo;
import com.syjy.tunnelinfo.gathertunnelinfo.GatherModbusTcpTunnelInfo;
import com.syjy.tunnelinfo.sendertunnelinfo.Sender104TcpTunnelInfo;
import com.syjy.tunnelinfo.sendertunnelinfo.SenderCdtRtuTunnelInfo;
import com.syjy.tunnelinfo.sendertunnelinfo.SenderModbusRtuTunnelInfo;
import com.syjy.tunnelinfo.sendertunnelinfo.SenderModbusTcpTunnelInfo;
import com.syjy.tunnelworker.BaseProtocolTunnel;
import com.syjy.tunnelworker.gathers.iml.CdtRtuDataGather;
import com.syjy.tunnelworker.gathers.iml.Iec104TcpDataGather;
import com.syjy.tunnelworker.gathers.iml.ModbusRtuDataGather;
import com.syjy.tunnelworker.gathers.iml.ModbusTcpDataGather;
import com.syjy.tunnelworker.senders.iml.CdtRtuDataSender;
import com.syjy.tunnelworker.senders.iml.Iec104TcpDataSender;
import com.syjy.tunnelworker.senders.iml.ModbusRtuDataSender;
import com.syjy.tunnelworker.senders.iml.ModbusTcpDataSender;

/**
 * 用Tunnel Info 创建通达
 *
 * @author: xiuwei
 * @version:
 */
public class TunnelBuilder {

	public static final Object RXTXSYNCLOCK = new Object();

	public static BaseProtocolTunnel buildTunnel(BaseTunnelInfo tunnelInfo) throws DataExchangeException {
		switch (tunnelInfo.getTunnelType()) {
			case IEC104TCPMASTER:
				return new Iec104TcpDataGather((Gather104TcpTunnelInfo) tunnelInfo).buildTunnel();
			case IEC104TCPSLAVE:
				return new Iec104TcpDataSender((Sender104TcpTunnelInfo) tunnelInfo).buildTunnel();
			case MODBUSRTUMASTER:
				return new ModbusRtuDataGather((GatherModbusRtuTunnelInfo) tunnelInfo).buildTunnel();
			case MODBUSTCPMASTER:
				return new ModbusTcpDataGather((GatherModbusTcpTunnelInfo) tunnelInfo).buildTunnel();
			case MODBUSRTUSLAVE:
				return new ModbusRtuDataSender((SenderModbusRtuTunnelInfo) tunnelInfo).buildTunnel();
			case MODBUSTCPSLAVE:
				return new ModbusTcpDataSender((SenderModbusTcpTunnelInfo) tunnelInfo).buildTunnel();
			case CDTRTUSLAVE:
				return new CdtRtuDataSender((SenderCdtRtuTunnelInfo) tunnelInfo).buildTunnel();
			case CDTRTUMASTER:
				return new CdtRtuDataGather((GatherCdtRtuTunnelInfo) tunnelInfo).buildTunnel();
			default:
				return null;
		}
	}


}


