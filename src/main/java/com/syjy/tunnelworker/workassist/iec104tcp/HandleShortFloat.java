package com.syjy.tunnelworker.workassist.iec104tcp;


import com.syjy.container.ProtocolDataContainer;
import com.syjy.tunnelinfo.DataPoint;
import wei.yigulu.iec104.annotation.AsduType;
import wei.yigulu.iec104.apdumodel.Apdu;
import wei.yigulu.iec104.asdudataframe.ShortFloatType;
import wei.yigulu.iec104.asdudataframe.qualitydescription.IeMeasuredQuality;
import wei.yigulu.iec104.asdudataframe.typemodel.InformationBodyAddress;
import wei.yigulu.iec104.nettyconfig.Iec104HSMasterBuilder;
import wei.yigulu.netty.BaseProtocolBuilder;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 处理短浮点
 *
 * @author 修唯xiuwei
 * @version 3.0
 */
@AsduType(typeId = 13)
public class HandleShortFloat extends ShortFloatType {

	private ProtocolDataContainer protocolDataContainer = ProtocolDataContainer.getInstance();

	/**
	 * 处理短浮点数据
	 */
	@Override
	public byte[][] handleAndAnswer(Apdu apdu) {
		apdu.getLog().trace("----------处理短浮点数据---------");
		BaseProtocolBuilder builder = apdu.getIec104Builder();
		if (builder != null && builder instanceof Iec104HSMasterBuilder) {
			Map<Integer, DataPoint> dataPoint = (Map<Integer, DataPoint>) builder.getConfigInfoMap().get("104DataPoint");
			HandleShortFloat handleShortFloat = (HandleShortFloat) apdu.getAsdu().getDataFrame();
			List<InformationBodyAddress> address = handleShortFloat.getAddresses();
			Map<IeMeasuredQuality, Float> dates = handleShortFloat.getDatas();
			int i = 0;
			//存入数据池
			if (apdu.getAsdu().getVsq().getSq() == 0) {
				apdu.getLog().info("------处理短浮点单一寻址-----");
				for (Map.Entry<IeMeasuredQuality, Float> e : dates.entrySet()) {
					if (dataPoint.containsKey(address.get(i).getAddress())) {
						protocolDataContainer.putNumber(dataPoint.get(address.get(i).getAddress()).getId(), BigDecimal.valueOf(e.getValue()).multiply(BigDecimal.valueOf(dataPoint.get(address.get(i).getAddress()).getMag())));
					}
					i++;
				}
			} else if (apdu.getAsdu().getVsq().getSq() == 1) {
				apdu.getLog().info("------处理短浮点连续寻址-----");
				i = address.get(0).getAddress();
				for (Map.Entry<IeMeasuredQuality, Float> e : dates.entrySet()) {
					if (dataPoint.containsKey(i)) {
						protocolDataContainer.putNumber(dataPoint.get(i).getId(), BigDecimal.valueOf(e.getValue()).multiply(BigDecimal.valueOf(dataPoint.get(i).getMag())));
					}
					i++;
				}
			}

		}
		return null;
	}

}
