package com.syjy.tunnelworker.senders;

import com.syjy.DataExchangeException;

/**
 * 协议转发数据通道
 *
 * @author 修唯xiuwei
 * @version 3.0
 */

public interface DataSenderInterface {

	/**
	 * 更新协议要发送的数据内容
	 *
	 * @throws DataExchangeException data exchange exception
	 */
	void updateData2Protocol() throws DataExchangeException;


}
