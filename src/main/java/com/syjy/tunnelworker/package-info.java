/**
 * 通道的工作者
 * 包括发送者和接受者
 * 以及发送者和接收者的接口
 * workassist 是通道创建使用过程中所需的辅助类
 *
 * @author 修唯xiuwei
 * @version 3.0
 */
package com.syjy.tunnelworker;

