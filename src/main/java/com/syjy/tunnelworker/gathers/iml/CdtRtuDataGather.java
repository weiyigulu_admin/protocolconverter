package com.syjy.tunnelworker.gathers.iml;

import com.syjy.DataExchangeException;
import com.syjy.container.ProtocolTunnelContainer;
import com.syjy.tunnelinfo.DataPoint;
import com.syjy.tunnelinfo.TunnelStatus;
import com.syjy.tunnelinfo.gathertunnelinfo.GatherCdtRtuTunnelInfo;
import com.syjy.tunnelworker.BaseProtocolTunnel;
import com.syjy.tunnelworker.gathers.DataGatherInterface;
import com.syjy.tunnelworker.workassist.SingleThreadPoolExecutorUtil;
import com.syjy.tunnelworker.workassist.cdt.MyCDTDataHandler;
import lombok.Getter;
import wei.yigulu.cdt.netty.CDTMaster;
import wei.yigulu.modbus.domain.datatype.ModbusDataTypeEnum;
import wei.yigulu.purejavacomm.PureJavaCommChannelConfig;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * cdt 协议 Rtu获取
 *
 * @author: xiuwei
 * @version:
 */
@SuppressWarnings("ALL")
public class CdtRtuDataGather extends BaseProtocolTunnel<GatherCdtRtuTunnelInfo,CDTMaster> implements DataGatherInterface {



	/**
	 * 遥测点表
	 */
	@Getter
	private Map<Integer, DataPoint> cdtYcDataPoints;
	/**
	 * 遥信点表
	 */
	@Getter
	private Map<Integer, DataPoint> cdtYxDataPoints;


	/**
	 * 构造方法
	 *
	 * @param gatherCdtRtuTunnelInfo 通道信息
	 */
	public CdtRtuDataGather(GatherCdtRtuTunnelInfo gatherCdtRtuTunnelInfo) {
		super(gatherCdtRtuTunnelInfo);
	}


	@Override
	public CdtRtuDataGather buildTunnel() throws DataExchangeException {
		parseGatherDataPoint();
		this.protocolBuilder = new CDTMaster(getTunnelInfo().getSerialName(), new MyCDTDataHandler(this).setLog(log));
		this.protocolBuilder.setLog(log);
		this.protocolBuilder.setBaudRate(getTunnelInfo().getBaudRate()).setParity(PureJavaCommChannelConfig.Paritybit.valueOf(getTunnelInfo().getParity()));
		this.setTunnelStatus(TunnelStatus.BUILT);
		ProtocolTunnelContainer.getInstance().addTunnel(this);
		log.info("成功创建CDT  rtu Master通道对象：{}", this.tunnelInfo.getTunnelName());
		return this;
	}

	@Override
	public void parseGatherDataPoint() throws DataExchangeException {
		List<DataPoint> dataPoints = this.getTunnelInfo().getDataPoints();
		cdtYxDataPoints = new HashMap<>();
		cdtYcDataPoints = new HashMap<>();
		if (dataPoints == null || dataPoints.size() == 0) {
			return;
		}
		for (DataPoint d : dataPoints) {
			if (ModbusDataTypeEnum.A16.equals(d.getDataType())) {
				cdtYxDataPoints.put(d.getProtocolPoint(), d);
			} else {
				cdtYcDataPoints.put(d.getProtocolPoint(), d);
			}
		}
	}


	@Override
	public BaseProtocolTunnel tunnelStop() throws DataExchangeException {
		if (this.protocolBuilder != null) {
			protocolBuilder.stop();
		}
		log.info("关闭 cdt Master 通道 {}", this.tunnelInfo.getTunnelName());
		return super.tunnelStop();
	}


	@Override
	public CdtRtuDataGather startTunnel() throws DataExchangeException {
		log.info("CDT RTU master {} 通道开始连接", getTunnelInfo().getTunnelName());
		protocolTunnelContainer.addUpdateDateTask(this);
		try {
			SingleThreadPoolExecutorUtil.executeBySingleThreadExecutor(() -> {
				this.protocolBuilder.create();
				log.error("cdt rtu master  创建通道失败");
				setTunnelStatus(TunnelStatus.LISTENSERIALFAIL);
			});
		} catch (Exception e) {
			log.error("cdt rtu master 创建通道失败", e);
			setTunnelStatus(TunnelStatus.LISTENSERIALFAIL);
			throw new DataExchangeException(10007, "cdt rtu master创建通道失败");
		}
		if (!TunnelStatus.LISTENSERIALFAIL.equals(getTunnelStatus())) {
			setTunnelStatus(TunnelStatus.LISTENSERIALSUCCESS);
		}
		return this;
	}

	@Override
	public void getDataFromProtocol() throws DataExchangeException {
	}


}
