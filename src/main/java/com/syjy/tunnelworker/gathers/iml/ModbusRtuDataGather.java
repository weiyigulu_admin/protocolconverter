package com.syjy.tunnelworker.gathers.iml;

import com.syjy.DataExchangeException;
import com.syjy.tunnelinfo.TunnelStatus;
import com.syjy.tunnelinfo.gathertunnelinfo.GatherModbusRtuTunnelInfo;
import com.syjy.tunnelworker.BaseProtocolTunnel;
import com.syjy.tunnelworker.workassist.SingleThreadPoolExecutorUtil;
import wei.yigulu.modbus.domain.FunctionCode;
import wei.yigulu.modbus.netty.ModbusRtuMasterBuilder;

/**
 * modbus RTU 采集通道
 *
 * @author: xiuwei
 * @version: 3.0
 */
public class ModbusRtuDataGather extends AbstractModbusDataGather<GatherModbusRtuTunnelInfo,ModbusRtuMasterBuilder> {


	/**
	 * 构造方法
	 *
	 * @param gatherModbusRtuTunnelInfo 通道信息
	 */
	public ModbusRtuDataGather(final GatherModbusRtuTunnelInfo gatherModbusRtuTunnelInfo) {
		super(gatherModbusRtuTunnelInfo);
	}

	@Override
	public ModbusRtuDataGather buildTunnel() throws DataExchangeException {
		protocolBuilder = new ModbusRtuMasterBuilder(this.tunnelInfo.getSerialName());
		protocolBuilder.setBaudRate(tunnelInfo.getBaudRate());
		protocolBuilder.setDataBits(tunnelInfo.getDataBits());
		protocolBuilder.setStopBits(tunnelInfo.getStopBits());
		protocolBuilder.setParity(tunnelInfo.getParity());
		protocolBuilder.setLog(this.log);
		setSlaveId(this.tunnelInfo.getSlaveId());
		setFunctionCode(FunctionCode.valueOf(this.tunnelInfo.getFunctionCode()));
		setTunnelStatus(TunnelStatus.BUILT);
		protocolTunnelContainer.addTunnel(this);
		parseGatherDataPoint();
		log.info("成功创建ModbusRTUMaster通道对象：{}", this.tunnelInfo.getTunnelName());
		return this;
	}



	@Override
	public ModbusRtuDataGather startTunnel() throws DataExchangeException {
		log.info("modbus RTU master {} 通道开始连接", getTunnelInfo().getTunnelName());
		protocolTunnelContainer.addUpdateDateTask(this);
		try {
			SingleThreadPoolExecutorUtil.executeBySingleThreadExecutor(() -> {
				try {
					this.protocolBuilder.create();
					log.error("Modbus Rtu Master  创建通道失败");
					setTunnelStatus(TunnelStatus.LISTENSERIALFAIL);
				} catch (Exception e) {
					log.error("Modbus Rtu Master  创建通道失败", e);
					setTunnelStatus(TunnelStatus.LISTENSERIALFAIL);
				}
			});
		} catch (Exception e) {
			log.error("Modbus Rtu Master 创建通道失败", e);
			setTunnelStatus(TunnelStatus.LISTENSERIALFAIL);
			throw new DataExchangeException(10007, "Modbus Rtu Master创建通道失败");
		}
		if (!TunnelStatus.LISTENSERIALFAIL.equals(getTunnelStatus())) {
			setTunnelStatus(TunnelStatus.LISTENSERIALSUCCESS);
		}
		return this;
	}


	@Override
	public BaseProtocolTunnel tunnelStop() throws DataExchangeException {
		if (this.protocolBuilder != null) {
			this.protocolBuilder.stop();
		}
		log.info("关闭 Modbus Rtu Master 通道 {}", this.tunnelInfo.getTunnelName());
		return super.tunnelStop();
	}


}


