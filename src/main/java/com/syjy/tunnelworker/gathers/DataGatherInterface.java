package com.syjy.tunnelworker.gathers;

import com.syjy.DataExchangeException;

/**
 * 通过接口采集数据的抽象类
 *
 * @author 修唯xiuwei
 * @version 3.0
 */
public interface DataGatherInterface {


	/**
	 * 该通道通过自身协议采集负责点位的数据并存入缓存池
	 *
	 * @throws DataExchangeException 自定义异常
	 */
	void getDataFromProtocol() throws DataExchangeException;


}
