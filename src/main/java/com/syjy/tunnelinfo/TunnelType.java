package com.syjy.tunnelinfo;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 通道的类型
 *
 * @author 修唯xiuwei
 * @version 3.0
 */
@Getter
@AllArgsConstructor
public enum TunnelType {


	/**
	 * 通道类型枚举
	 */
	MODBUSTCPMASTER("modbusTCP采集", "in"),
	/**
	 * modbusRTU采集
	 */
	MODBUSRTUMASTER("modbusRTU采集", "in"),

	/**
	 * modbusRTU采集
	 */
	MODBUSRTUMASTERWITHTCPSERVER("modbusRTU采集使用TCP通道", "in"),
	/**
	 * IEC104TCP采集
	 */
	IEC104TCPMASTER("IEC104TCP采集", "in"),
	/**
	 * CDTRTU采集
	 */
	CDTRTUMASTER("CDTRTU采集", "in"),
	/**
	 * modbusTCP转发
	 */
	MODBUSTCPSLAVE("modbusTCP转发", "out"),

	/**
	 * modbusRTU转发
	 */
	MODBUSRTUSLAVE("modbusRTU转发", "out"),
	/**
	 * IEC104TCP转发
	 */
	IEC104TCPSLAVE("IEC104TCP转发", "out"),
	/**
	 * CDTRTU转发
	 */
	CDTRTUSLAVE("CDTRTU转发", "out"),

	/**
	 *计算采集通道
	 */
	CALCULATOR("计算通道", "in"),


	/**
	 *生成隔离文件通道
	 */
	GENERATEFILE("生成隔离文件通道", "in");


	/**
	 * 通道的类型描述
	 */
	private String cnDescribe;


	/**
	 * 流向
	 */
	private String flow;


}
