package com.syjy.tunnelinfo.gathertunnelinfo;

import com.syjy.DataExchangeException;
import com.syjy.ReadConfigFile;
import com.syjy.tunnelinfo.BaseTunnelInfo;
import com.syjy.tunnelinfo.TunnelType;
import jxl.Cell;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * cdt协议rtu通道信息
 *
 * @author 修唯xiuwei
 * @version 3.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class GatherCdtRtuTunnelInfo extends BaseTunnelInfo {


	/**
	 * 串口名称
	 */
	private String serialName;
	/**
	 * 波特率
	 */
	private Integer baudRate;

	/**
	 * 奇偶校验
	 * PARITY_NONE = 0  无校验;
	 * PARITY_ODD = 1  奇校验;
	 * PARITY_EVEN = 2  偶校验;
	 * PARITY_MARK = 3  0校验;
	 * PARITY_SPACE = 4  1校验;
	 */
	private Integer parity = 0;

	/**
	 * 同步字1
	 */
	private Integer synchronousWord1 = 0xEB;
	/**
	 * 同步字2
	 */
	private Integer synchronousWord2 = 0x90;

	/**
	 * 初始化时确定通道类型
	 */
	public GatherCdtRtuTunnelInfo() {
		this.setTunnelType(TunnelType.CDTRTUMASTER);
	}


	@Override
	public void setByRow(Cell[] cells) throws DataExchangeException {
		this.setSerialName(ReadConfigFile.getCellContext(cells[0]));
		this.setBaudRate(Integer.parseInt(ReadConfigFile.getCellContext(cells[1])));
		this.setBaudRate(Integer.parseInt(ReadConfigFile.getCellContext(cells[2])));
	}
}
