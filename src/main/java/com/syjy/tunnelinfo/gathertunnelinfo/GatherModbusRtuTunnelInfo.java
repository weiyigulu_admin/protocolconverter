package com.syjy.tunnelinfo.gathertunnelinfo;

import com.syjy.DataExchangeException;
import com.syjy.ReadConfigFile;
import com.syjy.tunnelinfo.BaseTunnelInfo;
import com.syjy.tunnelinfo.TunnelType;
import jxl.Cell;
import lombok.Data;
import lombok.EqualsAndHashCode;
import wei.yigulu.purejavacomm.PureJavaCommChannelConfig;

/**
 * modbus协议rtu采集通道
 *
 * @author 修唯xiuwei
 * @version 3.0
 */
@EqualsAndHashCode(callSuper = true)
@Data

public class GatherModbusRtuTunnelInfo extends BaseTunnelInfo {


	/**
	 * 串口名称
	 */
	private String serialName;
	/**
	 * 波特率
	 */
	private Integer baudRate;
	/**
	 * 功能码
	 */
	private Integer functionCode = 3;
	/**
	 * 设备地址
	 */
	private Integer slaveId = 1;


	/**
	 * 数据位
	 */
	private PureJavaCommChannelConfig.Databits dataBits = PureJavaCommChannelConfig.Databits.DATABITS_8;
	/**
	 * 停止位
	 */
	private PureJavaCommChannelConfig.Stopbits stopBits = PureJavaCommChannelConfig.Stopbits.STOPBITS_1;
	/**
	 * 校验位
	 */
	private PureJavaCommChannelConfig.Paritybit parity = PureJavaCommChannelConfig.Paritybit.NONE;

	/**
	 * 初始化时确定通道类型
	 */
	public GatherModbusRtuTunnelInfo() {
		this.setTunnelType(TunnelType.MODBUSRTUMASTER);
	}


	@Override
	public void setByRow(Cell[] cells) throws DataExchangeException {
		this.setSerialName(ReadConfigFile.getCellContext(cells[0]));
		this.setBaudRate(Integer.parseInt(ReadConfigFile.getCellContext(cells[1])));
		this.setDataBits(PureJavaCommChannelConfig.Databits.valueOf(Integer.parseInt(ReadConfigFile.getCellContext(cells[2]))));
		this.setParity(PureJavaCommChannelConfig.Paritybit.valueOf(ReadConfigFile.getCellContext(cells[3])));
		this.setStopBits(PureJavaCommChannelConfig.Stopbits.valueOf(Integer.parseInt(ReadConfigFile.getCellContext(cells[4]))));
		this.setSlaveId(Integer.parseInt(ReadConfigFile.getCellContext(cells[5])));
		this.setFunctionCode(Integer.parseInt(ReadConfigFile.getCellContext(cells[6])));
	}
}
