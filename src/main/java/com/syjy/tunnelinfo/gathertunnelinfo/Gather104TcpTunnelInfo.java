package com.syjy.tunnelinfo.gathertunnelinfo;


import com.syjy.DataExchangeException;
import com.syjy.ReadConfigFile;
import com.syjy.tunnelinfo.BaseTunnelInfo;
import com.syjy.tunnelinfo.TunnelType;
import jxl.Cell;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 104协议tcp数据采集通道信息
 *
 * @author 修唯xiuwei
 * @version 3.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class Gather104TcpTunnelInfo extends BaseTunnelInfo {


	/**
	 * 远端ip
	 */
	private String remoteIp;

	/**
	 * 远端端口
	 */
	private Integer remotePort = 2404;

	/**
	 * 远端备用ip
	 */
	private String remoteSpareIp;

	/**
	 * 远端备用端口
	 */
	private Integer remoteSparePort;

	/**
	 * 自身的ip
	 */
	private String selfIp;

	/**
	 * 自身的端口
	 */
	private Integer selfPort;

	/**
	 * 公共地址位
	 */

	private Integer publicAddress = 1;

	/**
	 * 初始化时确定通道类型
	 */
	public Gather104TcpTunnelInfo() {
		this.setTunnelType(TunnelType.IEC104TCPMASTER);
	}


	@Override
	public void setByRow(Cell[] cells) throws DataExchangeException {
		this.setRemoteIp(ReadConfigFile.getCellContext(cells[0]));
		this.setRemotePort(Integer.parseInt(ReadConfigFile.getCellContext(cells[1])));
		try {
			this.setRemoteSpareIp(ReadConfigFile.getCellContext(cells[2]));
		}catch (DataExchangeException e){

		}
		try {
			this.setRemoteSparePort(Integer.parseInt(ReadConfigFile.getCellContext(cells[3])));
		}catch (DataExchangeException e){

		}
		this.setPublicAddress(Integer.parseInt(ReadConfigFile.getCellContext(cells[4])));
	}


}
