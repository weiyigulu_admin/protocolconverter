package com.syjy.tunnelinfo.gathertunnelinfo;

import com.syjy.DataExchangeException;
import com.syjy.tunnelinfo.BaseTunnelInfo;
import com.syjy.tunnelinfo.TunnelType;
import jxl.Cell;

/**
 * 点位计算器通道
 *
 * @author: xiuwei
 * @version:
 */
public class CalculatorTunnelInfo extends BaseTunnelInfo {

	public  CalculatorTunnelInfo(){
		this.setTunnelType(TunnelType.CALCULATOR);
	}

	@Override
	public void setByRow(Cell[] cells) throws DataExchangeException {

	}
}
