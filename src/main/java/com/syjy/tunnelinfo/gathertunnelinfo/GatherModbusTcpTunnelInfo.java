package com.syjy.tunnelinfo.gathertunnelinfo;


import com.syjy.DataExchangeException;
import com.syjy.ReadConfigFile;
import com.syjy.tunnelinfo.BaseTunnelInfo;
import com.syjy.tunnelinfo.TunnelType;
import jxl.Cell;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * modbus协议tcp采集通道
 *
 * @author 修唯xiuwei
 * @version 3.0
 */
@EqualsAndHashCode(callSuper = true)
@Data

public class GatherModbusTcpTunnelInfo extends BaseTunnelInfo {

	/**
	 * 初始化时确定通道类型
	 */
	public GatherModbusTcpTunnelInfo() {
		this.setTunnelType(TunnelType.MODBUSTCPMASTER);
	}

	/**
	 * 远端ip
	 */
	private String remoteIp;

	/**
	 * 远端端口
	 */
	private Integer remotePort = 502;

	/**
	 * 远端备用ip
	 */
	private String remoteSpareIp;

	/**
	 * 远端备用端口
	 */
	private Integer remoteSparePort;

	/**
	 * 自身的ip
	 */
	private String selfIp;

	/**
	 * 自身的端口
	 */
	private Integer selfPort;

	/**
	 * 功能码
	 */
	private Integer functionCode = 3;


	/**
	 * 设备地址
	 */
	private Integer slaveId = 1;


	@Override
	public void setByRow(Cell[] cells) throws DataExchangeException {
		this.setRemoteIp(ReadConfigFile.getCellContext(cells[0]));
		this.setRemotePort(Integer.parseInt(ReadConfigFile.getCellContext(cells[1])));
		try {
			this.setRemoteSpareIp(ReadConfigFile.getCellContext(cells[2]));
		}catch (DataExchangeException e){

		}
		try {
			this.setRemoteSparePort(Integer.parseInt(ReadConfigFile.getCellContext(cells[3])));
		}catch (DataExchangeException e){

		}
		this.setSlaveId(Integer.parseInt(ReadConfigFile.getCellContext(cells[4])));
		this.setFunctionCode(Integer.parseInt(ReadConfigFile.getCellContext(cells[5])));
	}
}
