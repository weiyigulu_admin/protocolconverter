package com.syjy.tunnelinfo;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 通道的状态
 *
 * @author 修唯xiuwei
 * @version 3.0
 */
@Getter
@AllArgsConstructor
public enum TunnelStatus {


	/**
	 * 通道状态枚举
	 */
	BUILT(0, "已创建"),
	/**
	 * 已连接
	 */
	CONNECTED(1, "已连接"),
	/**
	 * 被动丢失连接
	 */
	LOSECONN(2, "被动丢失连接"),
	/**
	 * 通道已主动关闭
	 */
	ACTIVECLOSE(3, "通道已主动关闭"),
	/**
	 * 正在尝试连接
	 */
	CONNECTING(4, "正在尝试连接"),


	/**
	 * listeningport 监听网口成功
	 */
	LISTENPORTSUCCESS(5, "监听端口成功"),

	/**
	 * listeningSerial 监听串口成功
	 */
	LISTENSERIALSUCCESS(6, "监听串口成功"),


	/**
	 * listeningport 监听网口失败
	 */
	LISTENPORTFAIL(7, "监听端口失败"),

	/**
	 * listeningSerial 监听串口失败
	 */
	LISTENSERIALFAIL(8, "监听串口失败"),

	/**
	 * listeningport 正在监听网口
	 */
	LISTENINGPORT(9, "正在监听端口"),

	/**
	 * listeningSerial 正在监听串口
	 */
	LISTENINGSERIAL(10, "正在监听串口"),

	/**
	 * listeningport 监听网口成功，并有客户端连接
	 */
	LISTENPORTSUCCESSANDCONN(13, "监听端口成功并有客户端连接"),

	/**
	 * listeningport 监听网口成功，并无客户端连接
	 */
	LISTENPORTSUCCESSANDNOCONN(14, "监听端口成功并无客户端连接"),

	/**
	 * listeningport 监听网口成功，并有客户端连接,且交互异常
	 */
	LISTENPORTSUCCESSANDCONNANDCOMMERROR(15, "监听端口成功并有客户端连接但交互异常"),

	/**
	 * listeningSerial 监听串口成功但交互异常
	 */
	LISTENSERIALSUCCESSANDCOMMERROR(16, "监听串口成功但交互异常"),


	/**
	 * 已连接,但是交互异常
	 */
	CONNECTEDANDCOMMERROR(17, "已连接但交互异常");


	/**
	 * 可用状态
	 */
	public static final List<TunnelStatus> ABLE_STATUS= Arrays.asList(
			TunnelStatus.LISTENSERIALSUCCESS,//监听串口正常
			TunnelStatus.LISTENPORTSUCCESSANDCONN, //监听端口成功，并由连接
			TunnelStatus.LISTENPORTSUCCESSANDCONNANDCOMMERROR,//监听端口成功，并由连接且交互异常
			TunnelStatus.LISTENSERIALSUCCESSANDCOMMERROR,//监听串口正常 且交互异常
			TunnelStatus.CONNECTED,//客户端 成功连接端
			TunnelStatus.CONNECTEDANDCOMMERROR//客户端 成功连接端 但交互异常
	);


	/**
	 * 正确的状态
	 */
	public static final List<TunnelStatus> CORRECT_STATUS= Arrays.asList(
			TunnelStatus.LISTENSERIALSUCCESS,//监听串口正常
			TunnelStatus.LISTENPORTSUCCESSANDCONN, //监听端口成功，并由连接
			TunnelStatus.CONNECTED//客户端 成功连接端
	);


	/**
	 * 通道的类型代号
	 */
	private Integer code;


	/**
	 * 通道的类型描述
	 */
	private String cnDescribe;


	public static List<TunnelStatus> getRunningProperlyStatus() {
		List<TunnelStatus> list = new ArrayList<>();
		list.add(TunnelStatus.LISTENSERIALSUCCESS);
		list.add(TunnelStatus.CONNECTED);
		list.add(TunnelStatus.LISTENPORTSUCCESS);
		return list;
	}

}
