package com.syjy.tunnelinfo;

import lombok.Data;
import wei.yigulu.modbus.domain.datatype.ModbusDataTypeEnum;

/**
 * 点位
 *
 * @author: xiuwei
 * @version:
 */
@Data
public class DataPoint {


	/**
	 * 在池子中的点位  也作为点位的唯一键
	 */
	Integer id;


	/**
	 * 通道中的点位
	 */
	Integer protocolPoint;


	/**
	 *计算公式
	 */
	String formula;


	/**
	 * 点位的类型
	 */
	ModbusDataTypeEnum dataType;


	/**
	 * 点位的倍率
	 */
	Double mag;


	/**
	 * 点位的备注描述
	 */
	String describe;


}
