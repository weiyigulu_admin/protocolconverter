package com.syjy.tunnelinfo.sendertunnelinfo;

import com.syjy.DataExchangeException;
import com.syjy.ReadConfigFile;
import com.syjy.tunnelinfo.BaseTunnelInfo;
import com.syjy.tunnelinfo.TunnelType;
import jxl.Cell;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * modbus协议tcp发送数据通道信息
 *
 * @author 修唯xiuwei
 * @version 3.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SenderModbusTcpTunnelInfo extends BaseTunnelInfo {


	/**
	 * 创建类时确定通道类型
	 */
	public SenderModbusTcpTunnelInfo() {
		this.setTunnelType(TunnelType.MODBUSTCPSLAVE);
	}


	/**
	 * 自身端口
	 */
	private Integer selfPort = 502;
	/**
	 * 自身ip
	 */
	private String selfIp;
	/**
	 * 功能码
	 */
	private Integer functionCode = 3;


	/**
	 * 设备地址
	 */
	private Integer slaveId = 1;


	@Override
	public void setByRow(Cell[] cells) throws DataExchangeException {
		try {
			this.setSelfIp(ReadConfigFile.getCellContext(cells[0]));
		} catch (DataExchangeException e) {
			this.setSelfIp(null);
		}
		this.setSelfPort(Integer.parseInt(ReadConfigFile.getCellContext(cells[1])));
		this.setSlaveId(Integer.parseInt(ReadConfigFile.getCellContext(cells[2])));
		this.setFunctionCode(Integer.parseInt(ReadConfigFile.getCellContext(cells[3])));
	}
}
