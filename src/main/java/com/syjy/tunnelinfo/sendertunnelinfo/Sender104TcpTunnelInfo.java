package com.syjy.tunnelinfo.sendertunnelinfo;

import com.syjy.DataExchangeException;
import com.syjy.ReadConfigFile;
import com.syjy.tunnelinfo.BaseTunnelInfo;
import com.syjy.tunnelinfo.TunnelType;
import jxl.Cell;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 104协议tcp发送数据通道信息
 *
 * @author 修唯xiuwei
 * @version 3.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class Sender104TcpTunnelInfo extends BaseTunnelInfo {


	/**
	 * 初始化时确定通道类型
	 */
	public Sender104TcpTunnelInfo() {
		this.setTunnelType(TunnelType.IEC104TCPSLAVE);
	}


	/**
	 * 本机端口
	 */
	private Integer selfPort = 2404;
	/**
	 * 本机ip
	 */
	private String selfIp;

	/**
	 * 公共地址位
	 */
	private Integer publicAddress = 1;


	@Override
	public void setByRow(Cell[] cells) throws DataExchangeException {
		try {
			this.setSelfIp(ReadConfigFile.getCellContext(cells[0]));
		} catch (DataExchangeException e) {
			this.setSelfIp(null);
		}
		this.setSelfPort(Integer.parseInt(ReadConfigFile.getCellContext(cells[1])));
		this.setPublicAddress(Integer.parseInt(ReadConfigFile.getCellContext(cells[2])));
	}
}
