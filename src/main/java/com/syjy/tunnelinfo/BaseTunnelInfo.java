package com.syjy.tunnelinfo;

import com.syjy.DataExchangeException;
import jxl.Cell;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 通道信息的基类，用于与数据库交互传递信息的实体
 *
 * @author 修唯xiuwei
 * @version 3.0
 */
@Data
public abstract class BaseTunnelInfo implements Serializable {

	/**
	 * 通道的id
	 */
	protected String id;
	/**
	 * 通道的名称
	 */

	protected String tunnelName;

	/**
	 * 通道的类型，冗余字段
	 */

	protected TunnelType tunnelType;

	/**
	 * 数据刷新时间间隔 单位s 秒
	 */

	protected Integer refreshInterval = 5;


	/**
	 * 通道中的点位
	 */
	List<DataPoint> dataPoints;


	public abstract void setByRow(Cell[] cells) throws DataExchangeException;


}
