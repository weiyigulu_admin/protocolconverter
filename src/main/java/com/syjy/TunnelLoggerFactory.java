package com.syjy;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.filter.ThresholdFilter;
import ch.qos.logback.core.ConsoleAppender;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.SizeAndTimeBasedFNATP;
import ch.qos.logback.core.rolling.TimeBasedRollingPolicy;
import ch.qos.logback.core.util.FileSize;
import org.slf4j.LoggerFactory;

/**
 * 通道logger的工厂  仅针对在slf4j 日志框架下 logback 实现
 * 修改 日志框架或实现时均需修改该类
 *
 * @author: xiuwei
 * @version:
 */
public class TunnelLoggerFactory {


	public static Logger getLogger(String tunnelId, String tunnelName) {
		//使用在slf4j的接口框架下获取 logback的 LoggerContext
		LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
		Logger logger = loggerContext.getLogger(tunnelId);

		//构建编码规则
		PatternLayoutEncoder encoder = new PatternLayoutEncoder();
		encoder.setContext(loggerContext);
		encoder.setPattern("%d{HH:mm:ss.SSS} [%thread] %-5level  %M-%L  - %msg%n");
		encoder.start();


		ConsoleAppender consoleAppender = new ConsoleAppender();
		consoleAppender.setContext(loggerContext);
		consoleAppender.setEncoder(encoder);
		ThresholdFilter levelFilterConsole = new ThresholdFilter();
		levelFilterConsole.setLevel("warn");
		levelFilterConsole.start();
		consoleAppender.addFilter(levelFilterConsole);
		consoleAppender.start();
		logger.addAppender(consoleAppender);

		//构建appender
		RollingFileAppender appender = new RollingFileAppender();
		appender.setContext(loggerContext);

		//构建过滤器
		ThresholdFilter levelFilter = new ThresholdFilter();
		levelFilter.setLevel("trace");
		levelFilter.start();
		appender.addFilter(levelFilter);

		//构建滚动规则
		TimeBasedRollingPolicy rollingPolicyBase = new TimeBasedRollingPolicy<>();
		rollingPolicyBase.setContext(loggerContext);
		rollingPolicyBase.setParent(appender);
		System.out.println("日志打印路径  "+FileUtils.getResourcesPath() + "/logs/" );
		rollingPolicyBase.setFileNamePattern(FileUtils.getResourcesPath() + "/logs/" + tunnelName + ".%d{yyyy-MM-dd}.%i.log");
		//设定滚动触发器
		SizeAndTimeBasedFNATP sizeAndTimeBasedFNATP = new SizeAndTimeBasedFNATP();
		sizeAndTimeBasedFNATP.setMaxFileSize(FileSize.valueOf("100MB"));
		rollingPolicyBase.setTimeBasedFileNamingAndTriggeringPolicy(sizeAndTimeBasedFNATP);
		//最大文件数
		rollingPolicyBase.setMaxHistory(10);
		rollingPolicyBase.setCleanHistoryOnStart(true);
		rollingPolicyBase.setTotalSizeCap(FileSize.valueOf("1GB"));
		rollingPolicyBase.start();

		appender.setEncoder(encoder);
		appender.setRollingPolicy(rollingPolicyBase);
		appender.start();

		logger.setAdditive(false);
		logger.addAppender(appender);
		return logger;
	}

	/**
	 * 关闭该logger
	 *
	 * @param logger
	 */
	public static void stop(Logger logger) {
		LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
		loggerContext.removeObject(logger.getName());
		logger.detachAndStopAllAppenders();
	}
}
