package com.syjy;

import com.syjy.tunnelinfo.BaseTunnelInfo;
import com.syjy.tunnelinfo.DataPoint;
import com.syjy.tunnelinfo.gathertunnelinfo.*;
import com.syjy.tunnelinfo.sendertunnelinfo.Sender104TcpTunnelInfo;
import com.syjy.tunnelinfo.sendertunnelinfo.SenderCdtRtuTunnelInfo;
import com.syjy.tunnelinfo.sendertunnelinfo.SenderModbusRtuTunnelInfo;
import com.syjy.tunnelinfo.sendertunnelinfo.SenderModbusTcpTunnelInfo;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import org.apache.commons.lang3.StringUtils;
import wei.yigulu.modbus.domain.datatype.ModbusDataTypeEnum;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * 读取配置表格
 *
 * @author: xiuwei
 * @version:
 */
public class ReadConfigFile {


	public static List<BaseTunnelInfo> readConfigFile() throws IOException, BiffException {
		List<BaseTunnelInfo> tunnelInfos = new ArrayList<>();
		File configFile = new File(FileUtils.getResourcesPath() + "/pointconfig.xls");
		InputStream is = new FileInputStream(configFile);
		Workbook wb = Workbook.getWorkbook(is);
		Sheet[] sheets = wb.getSheets();
		String cellValue;
		BaseTunnelInfo tunnelInfo;
		List<DataPoint> dataPoints;
		DataPoint dataPoint;
		ModbusDataTypeEnum dataType;
		int sheetI = 1;
		for (Sheet sheet : sheets) {
			try {
				cellValue = getCellContext(sheet.getCell(1, 0));
				switch (cellValue) {
					case "104M":
						tunnelInfo = new Gather104TcpTunnelInfo();
						break;
					case "104S":
						tunnelInfo = new Sender104TcpTunnelInfo();
						break;
					case "ModbusTCPM":
						tunnelInfo = new GatherModbusTcpTunnelInfo();
						break;
					case "ModbusRTUM":
						tunnelInfo = new GatherModbusRtuTunnelInfo();
						break;
					case "CDTM":
						tunnelInfo = new GatherCdtRtuTunnelInfo();
						break;
					case "ModbusTCPS":
						tunnelInfo = new SenderModbusTcpTunnelInfo();
						break;
					case "ModbusRTUS":
						tunnelInfo = new SenderModbusRtuTunnelInfo();
						break;
					case "CDTS":
						tunnelInfo = new SenderCdtRtuTunnelInfo();
						break;
					case "Calculator" :
						tunnelInfo=new CalculatorTunnelInfo();break;
					default:
						throw new DataExchangeException(4002, "通道类型异常");
				}
				tunnelInfo.setId(sheetI + "");
				tunnelInfo.setTunnelName(sheet.getName() + sheetI);
				tunnelInfo.setByRow(sheet.getRow(2));
				tunnelInfo.setRefreshInterval(Integer.parseInt(getCellContext(sheet.getCell(3, 0))));
				dataPoints = new ArrayList<>();
				for (int i = 4; i < sheet.getRows(); i++) {
					dataPoint = new DataPoint();
					try {
						dataPoint.setId(Integer.parseInt(getCellContext(sheet.getCell(0, i))));
					}catch (DataExchangeException e){
						continue;
					}
					if("Calculator".equals(getCellContext(sheet.getCell(1, 0)))){
						dataPoint.setFormula(getCellContext(sheet.getCell(1, i)));
					}else {
						dataPoint.setProtocolPoint(Integer.parseInt(getCellContext(sheet.getCell(1, i))));
					}
					cellValue = getCellContext(sheet.getCell(2, i));
					if ("遥信".equals(cellValue)) {
						dataType = ModbusDataTypeEnum.A16;
					} else if ("遥测".equals(cellValue)) {
						dataType = ModbusDataTypeEnum.ABCD;
					} else {
						dataType = ModbusDataTypeEnum.valueOf(cellValue);
					}
					dataPoint.setDataType(dataType);
					dataPoint.setMag(Double.parseDouble(getCellContext(sheet.getCell(3, i))));
					dataPoint.setDescribe(sheet.getCell(4, i).getContents());
					dataPoints.add(dataPoint);
				}
				tunnelInfo.setDataPoints(dataPoints);
				tunnelInfos.add(tunnelInfo);
			} catch (Exception e) {
				e.printStackTrace();
			}
			sheetI++;
		}
		return tunnelInfos;
	}


	public static String getCellContext(Cell cell) throws DataExchangeException {
		if (cell == null) {
			throw new DataExchangeException(4001, "配置文件缺少配置内容," + cell.getRow() + "行" + cell.getColumn() + "列");
		}
		String val = cell.getContents();
		if (StringUtils.isNoneEmpty(val)) {
			return val;
		} else {
			throw new DataExchangeException(4001, "配置文件缺少配置内容," + cell.getRow() + "行" + cell.getColumn() + "列");
		}
	}


}
