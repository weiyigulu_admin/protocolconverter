package com.syjy;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * dataexchange 模块中的异常
 *
 * @author 修唯xiuwei
 * @version 3.0
 */
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Data
public class DataExchangeException extends Exception {


	/**
	 * 构造方法
	 *
	 * @param code 异常码
	 * @param msg  异常信息
	 */
	public DataExchangeException(Integer code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	/**
	 * 异常码
	 */
	Integer code;

	/**
	 * 异常信息
	 */
	String msg;

	/**
	 * 异常的具体信息
	 */
	String localMsg;


}
